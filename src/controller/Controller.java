package controller;

import api.IDivvyTripsManager;
import model.data_structures.DoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {
		manager.loadStations("C:/Users/santi/Talleres/t2_201820/data/Divvy_Stations_2017_Q3Q4.csv"); //Si utilizaba la direccion del archivo como /t2_201820/data/Divvy_Stations_2017_Q3Q4.csv me aparecia FileNotFound exception. Esta fue la unica manera en la que logre qu elo encontrara
	}
	
	public static void loadTrips() {
		manager.loadTrips("C:/Users/santi/Talleres/t2_201820/data/Divvy_Trips_2017_Q4.csv"); //Lo mismo que para stations
	}
		
	public static DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		return manager.getTripsOfGender(gender);
	}
	
	public static DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		return manager.getTripsToStation(stationID);
	}
}
