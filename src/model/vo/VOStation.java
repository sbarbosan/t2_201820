package model.vo;

public class VOStation {

	public int stationId;
	public String name;
	public String city;
	public double latitude;
	public double longitude;
	public int dpcapacity;
	public String onlineDate;
	
	public VOStation(int id, String n, String c, double lat, double lon, int cap, String online)
	{
		stationId = id;
		name = n; 
		city = c;
		latitude = lat;
		longitude = lon;
		dpcapacity = cap;
		onlineDate = online;
	}
	
	public int getId()
	{
		return stationId;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getCity()
	{
		return city;
	}
}
