package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	public int tripId; 
	public String startTime;
	public String endTime;
	public int bikeId;
	public double tripDuration;
	public int idFromStation;
	public String fromStation;
	public int idToStation;
	public String toStation;
	public String type;
	public String gender;
	public int birthyear;
	

	public VOTrip(int id, String start, String end, int bId, double dur, int idFrom, String from, int idTo, String to, String t, String g, int b)
	{
		tripId = id;
		startTime = start; 
		endTime = end;
		bikeId = bId;
		tripDuration = dur;
		idFromStation = idFrom;
		fromStation = from;
		idToStation = idTo;
		toStation = to; 
		type = t;
		gender = g; 
		birthyear = b;
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return tripId;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return tripDuration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return fromStation;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return toStation;
	}
}
