package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import api.IDivvyTripsManager;
import model.data_structures.DoublyLinkedList;
import model.data_structures.DoublyLinkedNodeList;
import model.data_structures.Node;
import model.vo.VOStation;
import model.vo.VOTrip;

public class DivvyTripsManager<T> implements IDivvyTripsManager {

	DoublyLinkedNodeList<T> tripList;
	DoublyLinkedNodeList<T> stationsList;
	public DivvyTripsManager()
	{
		tripList = new DoublyLinkedNodeList<>();
		stationsList = new DoublyLinkedNodeList<>();
	}
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub
		try 
		{
			int contador = 1;
			FileReader fileReader = new FileReader(stationsFile);
			BufferedReader reader = new BufferedReader(fileReader);

			reader.readLine();
			String linea = reader.readLine();
			
			while(linea != null)
			{
				
				String[] lineas = linea.split(",");
				int id = Integer.parseInt(lineas[0]);
				String name = lineas[1];
				String city = lineas[2];
				double latitude = Double.parseDouble(lineas[3]);
				double longitude = Double.parseDouble(lineas[4]);
				int dpcapacity = Integer.parseInt(lineas[5]);
				String online_date = lineas[6];
				VOStation station = new VOStation(id, name, city, latitude, longitude, dpcapacity, online_date);
				stationsList.addNode(new Node(station));
				System.out.println(contador);
				linea = reader.readLine();
				contador ++;
			}
			
			reader.close();
			fileReader.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
	}

	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		try 
		{
			int contador = 1;
			FileReader fileReader = new FileReader(tripsFile);
			BufferedReader reader = new BufferedReader(fileReader);

			reader.readLine();
			String linea = reader.readLine();
			
			while(linea != null)
			{
				
				String[] lineas = linea.split(",");
				
				int id = Integer.parseInt(lineas[0]);
				String start = lineas[1];
				String end = lineas[2];
				int bId = Integer.parseInt(lineas[3]);
				double dur = Double.parseDouble(lineas[4]);
				int idF = Integer.parseInt(lineas[5]);
				String fr = lineas[6];
				int idT = Integer.parseInt(lineas[7]);
				String to = lineas[8];
				String t = lineas[9];
				String g = "";
				int b = 0;
				if(lineas.length > 10)
				{
					 g = lineas[10];
				}
				if(lineas.length > 11)
				{

					b = Integer.parseInt(lineas[11]);
				}
				
				VOTrip trip = new VOTrip(id, start, end, bId, dur, idF, fr, idT, to, t, g, b);
				tripList.addNode(new Node(trip));
				System.out.println(contador);
				linea = reader.readLine();
				contador ++;
			}
			
			reader.close();
			fileReader.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
	}
	
	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		DoublyLinkedList gList = new DoublyLinkedNodeList<>();
		if(tripList.first != null)
		{
			for(Node x = tripList.first; x != null; x = x.next)
			{
				VOTrip trip = (VOTrip) x.element;
				if(trip.gender.equals(gender))
				{
					gList.addNode(new Node(trip));
				}
			}
		}
	
		return gList;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		DoublyLinkedList sList = new DoublyLinkedNodeList<>();
		if(tripList.first != null)
		{
			
			for(Node x = tripList.first; x != null; x = x.next)
			{
				VOTrip trip = (VOTrip) x.element;
				if(trip.idToStation == stationID)
				{
					sList.addNode(new Node(trip));
				}
			}
		}
	
		return sList;
	}	


}
