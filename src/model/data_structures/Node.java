package model.data_structures;

public class Node<T> {

	public T element;
	public Node<T> next;
	public Node<T> previous;
	
	public Node(T i)
	{
		element = i;
		
	}
	
	public T giveElement()
	{
		return element;
	}
}
