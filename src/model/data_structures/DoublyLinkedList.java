package model.data_structures;


/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> {


	Integer getSize();
	
	void addNode(Node<T> n);
	
	void  addAtEnd(Node<T> n);
	
	void AddAtK(Node<T> n, int i);
	
	Node<T> getElement();
	
	

}
