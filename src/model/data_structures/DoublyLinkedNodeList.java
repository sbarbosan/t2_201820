package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedNodeList<T> implements DoublyLinkedList<T>{

	public Node first;
	public Node last;
	
	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	public Node getFirstt() {
		// TODO Auto-generated method stub
		return first;
	}
	
	@Override
	public Integer getSize() {
		int contador = 0; 
		if(first == null)
		{
			return contador;			
		}
		else
		{
			for(Node x = first; x != null; x = x.next)
			{
				contador++;
			}
			return contador;
		}
	}

	@Override
	public void addNode(Node n) {
		if(first != null)
		{
			first.previous = n;
			n.next = first;
		}
		else
		{
			last = n;
		}
		first = n;
	}

	@Override
	public void addAtEnd(Node n) {
		if(last != null)
		{
			last.next = n; 
			n.previous = n;
		}
		else 
		{
			first = n;
			last = n;
		}
		
		last = n;
	}

	@Override
	public void AddAtK(Node n, int k) {
		// TODO Auto-generated method stub
		if(first == null)
		{
			first = n;
		}
		else
		{
			Node prev = last;
			int contador = 0;
			for(Node x = first; x != null && contador < k; x = x.next )
			{
				contador ++;
				if(x != null)
				{
					prev = x;
				}
			}
			
			n.next = prev.next;
			prev.next.previous = n;
			prev.next = n;
			n.previous = prev;
		}	
		
	}

	@Override
	public Node getElement() {
		// TODO Auto-generated method stub
		return first;
	}

}
